<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testUser(): void
    {
        $user = new User();
        $user->setEmail('john@doe.com');
        $user->setFirstName('john');
        $user->setLastName('doe');
        $user->setPassword('azerty');
        $this->assertTrue($user->getEmail() === 'john@doe.com');
    }
}
