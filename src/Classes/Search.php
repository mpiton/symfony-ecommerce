<?php

namespace App\Classes;

use App\Entity\Category;

class Search
{
    /**
     * @var string
     */
    public string $string;
    /**
     * @var Category[]
     */
    public $categories = [];
}
