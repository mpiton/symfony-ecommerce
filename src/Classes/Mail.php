<?php

namespace App\Classes;

use Mailjet\Client;
use Mailjet\Resources;

class Mail
{
    private $api_key = '32de9452979a3dda142c39a529c6f066';
    private $api_key_secret = '02cc1693d8ee6b0fc2c6e15429567015';

    public function send($to_email, $to_name, $subject, $content)
    {
        $mj = new Client($this->api_key, $this->api_key_secret, true, [
            'version' => 'v3.1',
        ]);
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => 'mathieu-piton@4o4.fr',
                        'Name' => 'Boutique 4o4',
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,
                            'Name' => $to_name,
                        ],
                    ],
                    'TemplateID' => 2644494,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content,
                    ],
                ],
            ],
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);
        $response->success();
    }
}
