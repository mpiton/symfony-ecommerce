<?php

namespace App\Classes;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Cart
{
    private $session;
    private $entityManager;
    public function __construct(
        EntityManagerInterface $entityManager,
        SessionInterface $session
    ) {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }
    public function add($id)
    {
        //search the cart session
        $cart = $this->session->get('cart', []);
        // if my cart is not empty with this $id ---> add one more of this
        if (!empty($cart[$id])) {
            $cart[$id]++;
        } else {
            //else there is only one of this product
            $cart[$id] = 1;
        }
        $this->session->set('cart', $cart);
    }
    public function get()
    {
        return $this->session->get('cart');
    }
    public function remove()
    {
        return $this->session->remove('cart');
    }
    public function delete($id)
    {
        $cart = $this->session->get('cart', []);
        unset($cart[$id]);
        return $this->session->set('cart', $cart);
    }
    public function decrease($id)
    {
        //search the cart session
        $cart = $this->session->get('cart', []);
        // if there is more than one of this product
        if ($cart[$id] > 1) {
            $cart[$id]--;
        } else {
            //else delete the product from cart
            unset($cart[$id]);
        }
        $this->session->set('cart', $cart);
    }
    public function getFullCart()
    {
        $cartComplete = [];
        if ($this->get()) {
            foreach ($this->get() as $id => $quantity) {
                $productObject = $this->entityManager
                    ->getRepository(Product::class)
                    ->findOneBy(['id' => $id]);
                //if the product does not exist ---> delete this product from cart & continue
                if (!$productObject) {
                    $this->delete($id);
                    continue;
                }
                $cartComplete[] = [
                    'product' => $productObject,
                    'quantity' => $quantity,
                ];
            }
        }
        return $cartComplete;
    }
}
