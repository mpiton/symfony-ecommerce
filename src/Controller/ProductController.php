<?php

namespace App\Controller;

use App\Classes\Search;
use App\Entity\Product;
use App\Form\SearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/nos-produits", name="products")
     */
    public function index(Request $request): Response
    {
        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $this->entityManager
                ->getRepository(Product::class)
                ->findWithSearch($search);
        } else {
            $products = $this->entityManager
                ->getRepository(Product::class)
                ->findAll();
        }

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/produit/{slug}", name="product")
     */
    public function show(string $slug): Response
    {
        $product = $this->entityManager
            ->getRepository(Product::class)
            ->findOneBy([
                'slug' => $slug,
            ]);
        $products = $this->entityManager
            ->getRepository(Product::class)
            ->findBy(['isBest' => true]);
        //if no product -> redirect to products
        if (!$product) {
            return $this->redirectToRoute('products');
        }

        // else show product
        return $this->render('product/show.html.twig', [
            'product' => $product,
            'products' => $products,
        ]);
    }
}
