<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Classes\Mail;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderSuccessController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/commande/merci/{stripeSessionId}", name="order_validate")
     */
    public function index(Cart $cart, $stripeSessionId): Response
    {
        $order = $this->entityManager
            ->getRepository(Order::class)
            ->findOneBy(['stripeSessionId' => $stripeSessionId]);
        if (!$order || $order->getUser() != $this->getUser()) {
            return $this->redirectToRoute('home');
        }
        if ($order->getState() == 0) {
            //payment done -> remove cart
            $cart->remove();
            $order->setState(1);
            $this->entityManager->flush();

            //envoi de mail de confirmation de commande
            $mail = new Mail();
            $content =
                'Bonjour ' .
                $order->getUser()->getFirstname() .
                '<br>Merci pour votre commande sur la Boutique 4o4.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
            $mail->send(
                $order->getUser()->getEmail(),
                $order->getUser()->getFirstname(),
                'Votre commande sur Boutique 4o4 est bien validée.',
                $content
            );
        }

        return $this->render('order_success/index.html.twig', [
            'order' => $order,
        ]);
    }
}
