<?php

namespace App\Controller;

use App\Classes\Mail;
use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    private $entityManager;

    /**
     * Call Doctrine
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/inscription", name="register")
     */
    public function index(
        Request $request,
        UserPasswordEncoderInterface $encoder
    ): Response {
        $notification_error = null;
        $notification_success = null;
        $user = new User();
        //Création du formulaire
        $form = $this->createForm(RegisterType::class, $user);

        //Vérifie la request
        $form->handleRequest($request);

        //Si les données sont soumises & qu'elles sont valides
        if ($form->isSubmitted() && $form->isValid()) {
            //récupération des données du formulaire
            $user = $form->getData();

            //cherche une correspondance avec un email déjà existant
            $search_email = $this->entityManager
                ->getRepository(User::class)
                ->findOneBy(['email' => $user->getEmail()]);
            //si l'email n'existe pas en BDD
            if (!$search_email) {
                //cryptage du mot de passe
                $password = $encoder->encodePassword(
                    $user,
                    $user->getPassword()
                );

                //reinjecte password dans $user
                $user->setPassword($password);

                //enregistrement en BDD
                $this->entityManager->persist($user); //fige la data
                $this->entityManager->flush(); //execution
                //envoi de mail de confirmation
                $mail = new Mail();
                $content =
                    'Bonjour ' .
                    $user->getFirstname() .
                    '<br>Bienvenue sur la première boutique dédié au Made in France.<br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
                $mail->send(
                    $user->getEmail(),
                    $user->getFirstname(),
                    'Bienvenue sur la boutique 4o4',
                    $content
                );
                $notification_success =
                    "Votre inscription s'est correctement déroulée, vous pouvez dès à présent vous connecter à votre compte.";
            } else {
                //envoi de la notification d'erreur
                $notification_error =
                    "L'email que vous avez renseigné existe déjà.";
            }
        }

        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'notification_error' => $notification_error,
            'notification_success' => $notification_success,
        ]);
    }
}
