<?php

namespace App\Controller;

use App\Classes\Mail;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    private $email = 'mathieu-piton@4o4.fr';

    /**
     * @Route("/nous-contacter", name="contact")
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash(
                'notice',
                'Merci de nous avoir contacté. Notre équipe va vous répondre dans les meilleurs délais.'
            );
            $mail = new Mail();
            $mail->send(
                $this->email,
                'Boutique 4o4',
                'Nouvelle demande de contact de ' .
                    $form['firstname']->getData() .
                    ' ' .
                    $form['lastname']->getData(),
                '<strong>Contenu du message:</strong> <br>' .
                    $form['content']->getData() .
                    '<br><strong>Email du destinataire: ' .
                    $form['email']->getData() .
                    '</strong>'
            );
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
