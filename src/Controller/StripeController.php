<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeController extends AbstractController
{
    /**
     * @Route("/commande/create-session/{reference}", name="stripe_create_session")
     */
    public function index(EntityManagerInterface $entityManager, $reference)
    {
        //Variable for using stripe
        $product_for_stripe = [];
        $YOUR_DOMAIN = 'http://127.0.0.1:8000';

        $order = $entityManager
            ->getRepository(Order::class)
            ->findOneBy(['reference' => $reference]);
        if (!$order) {
            dd($order);
            new JsonResponse(['error' => 'order']);
        }
        //PAYMENT //////////////////////////////////
        //register my product -> OrderDetails()
        foreach ($order->getOrderDetails()->getValues() as $product) {
            $product_object = $entityManager
                ->getRepository(Product::class)
                ->findOneBy(['name' => $product->getProduct()]);
            $product_for_stripe[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $product->getPrice(),
                    'product_data' => [
                        'name' => $product->getProduct(),
                        'images' => [
                            $YOUR_DOMAIN .
                            '/uploads/' .
                            $product_object->getIllustration(),
                        ],
                    ],
                ],
                'quantity' => $product->getQuantity(),
            ];
        }
        $product_for_stripe[] = [
            'price_data' => [
                'currency' => 'eur',
                'unit_amount' => $order->getCarrierPrice(),
                'product_data' => [
                    'name' => $order->getCarrierName(),
                    'images' => [$YOUR_DOMAIN],
                ],
            ],
            'quantity' => 1,
        ];
        Stripe::setApiKey(
            'sk_test_51ITkA8AbcQsITn9nBG7LoBzf5OiLfzjlfaUmuuxM6F1Mc67brRCpwhQsW2SDGZViKfigJgytDky4ZQd1s7ita3uU00FF7qUYNO'
        );
        $checkout_session = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'payment_method_types' => ['card'],
            'line_items' => [$product_for_stripe],
            'mode' => 'payment',
            'success_url' => $YOUR_DOMAIN . '/commande/merci/{CHECKOUT_SESSION_ID}',
            'cancel_url' => $YOUR_DOMAIN . '/commande/erreur/{CHECKOUT_SESSION_ID}',
        ]);
        $order->setStripeSessionId($checkout_session->id);
        $entityManager->flush();
        $response = new JsonResponse(['id' => $checkout_session->id]);

        return $response;
    }
}
