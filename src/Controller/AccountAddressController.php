<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Address;
use App\Form\AddressType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountAddressController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/compte/addresses", name="account_address")
     */
    public function index(): Response
    {
        return $this->render('account/address.html.twig');
    }

    /**
     * @Route("/compte/ajouter-une-adresse", name="add_account_address")
     */
    public function add(Cart $cart, Request $request): Response
    {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);
        //if submit & valid
        if ($form->isSubmitted() && $form->isValid()) {
            //link adress with the user
            $address->setUser($this->getUser());
            //fix my data
            $this->entityManager->persist($address);
            //register in db
            $this->entityManager->flush();
            if ($cart->get()) {
                return $this->redirectToRoute('cart');
            } else {
                return $this->redirectToRoute('account_address');
            }
        }

        return $this->render('account/form_address.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/compte/modifier-une-adresse/{id}", name="edit_account_address")
     */
    public function edit(Request $request, $id): Response
    {
        $address = $this->entityManager
            ->getRepository(Address::class)
            ->findOneBy(['id' => $id]);
        if (!$address || $address->getUser() != $this->getUser()) {
            return $this->redirectToRoute('account_address');
        }
        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);
        //if submit & valid
        if ($form->isSubmitted() && $form->isValid()) {
            //register in db
            $this->entityManager->flush();

            return $this->redirectToRoute('account_address');
        }

        return $this->render('account/form_address.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/compte/supprimer-une-adresse/{id}", name="delete_account_address")
     */
    public function delete($id): Response
    {
        $address = $this->entityManager
            ->getRepository(Address::class)
            ->findOneBy(['id' => $id]);
        if ($address && $address->getUser() == $this->getUser()) {
            $this->entityManager->remove($address);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('account_address');
    }
}
