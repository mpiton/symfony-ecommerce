<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use App\Classes\Mail;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;

class OrderCrudController extends AbstractCrudController
{
    private $entityManager;
    private $crudUrlGenerator;
    public function __construct(
        EntityManagerInterface $entityManager,
        CrudUrlGenerator $crudUrlGenerator
    ) {
        $this->crudUrlGenerator = $crudUrlGenerator;
        $this->entityManager = $entityManager;
    }
    public static function getEntityFqcn(): string
    {
        return Order::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        $updatePreparation = Action::new(
            'updatePreparation',
            'Préparation en cours',
            'fas fa-box-open'
        )->linkToCrudAction('updatePreparation');
        $updateDelivery = Action::new(
            'updateDelivery',
            'Livraison en cours',
            'fas fa-truck'
        )->linkToCrudAction('updateDelivery');
        $updateFinish = Action::new(
            'updateFinish',
            'Commande livrée',
            'fas fa-check-circle'
        )->linkToCrudAction('updateFinish');
        return $actions
            ->add('detail', $updatePreparation)
            ->add('detail', $updateDelivery)
            ->add('detail', $updateFinish)
            ->add('index', 'detail');
    }

    //Commande en préparation
    public function updatePreparation(AdminContext $context)
    {
        $order = $context->getEntity()->getInstance();
        $order->setState(2);
        $this->entityManager->flush();
        $this->addFlash(
            'notice',
            '<span style="color:green;"><strong>La commande' .
                $order->getReference() .
                ' est bien <u>en cours de préparation<u></strong></span>'
        );
        $url = $this->crudUrlGenerator
            ->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        $mail = new Mail();
        $mail->send(
            $order->getUser()->getEmail(),
            $order->getUser()->getFirstname(),
            'Commande en cours de préparation',
            'Nous vous informons que votre commande est entrain d\'être préparé pour être livrée.'
        );
        return $this->redirect($url);
    }

    //Commande en cours de livraison
    public function updateDelivery(AdminContext $context)
    {
        $order = $context->getEntity()->getInstance();
        $order->setState(3);
        $this->entityManager->flush();
        $this->addFlash(
            'notice',
            '<span style="color:orange;"><strong>La commande' .
                $order->getReference() .
                ' est bien <u>en cours de livraison<u></strong></span>'
        );
        $url = $this->crudUrlGenerator
            ->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        $mail = new Mail();
        $mail->send(
            $order->getUser()->getEmail(),
            $order->getUser()->getFirstname(),
            'Commande en cours livraison',
            'Nous vous informons que votre commande est partie de nos entrpôts, elle arrivera bientôt chez vous !'
        );
        return $this->redirect($url);
    }

    //Commande livrée
    public function updateFinish(AdminContext $context)
    {
        $order = $context->getEntity()->getInstance();
        $order->setState(4);
        $this->entityManager->flush();
        $this->addFlash(
            'notice',
            '<span style="color:orange;"><strong>La commande' .
                $order->getReference() .
                ' <u>a bien été livrée<u></strong></span>'
        );
        $url = $this->crudUrlGenerator
            ->build()
            ->setController(OrderCrudController::class)
            ->setAction('index')
            ->generateUrl();
        return $this->redirect($url);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['id' => 'desc']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            DateTimeField::new('createdAt', 'Commandé le'),
            TextField::new('user.getFullName', 'Utilisateur'),
            TextEditorField::new(
                'delivery',
                'Adresse de livraison'
            )->onlyOnDetail(),
            MoneyField::new('total', 'Total de la commande')->setCurrency(
                'EUR'
            ),
            TextField::new('carrierName', 'Transporteur'),
            MoneyField::new('carrierPrice', 'Frais de port')->setCurrency(
                'EUR'
            ),
            ChoiceField::new('state', 'Statut')->setChoices([
                'Non payée' => 0,
                'Payée' => 1,
                'Préparation en cours' => 2,
                'Livraison en cours' => 3,
                'Commande livrée' => 4,
            ]),
            ArrayField::new('orderDetails', 'Produits achetés')->hideOnIndex(),
        ];
    }
}
