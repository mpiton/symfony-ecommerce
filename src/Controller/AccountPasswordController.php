<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AccountPasswordController extends AbstractController
{
    private $entityManager;

    /**
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/compte/mot-de-passe", name="account_password")
     */
    public function index(
        Request $request,
        UserPasswordEncoderInterface $encoder
    ): Response {
        $notification = null;
        $success = false;
        //récupération des info du compte
        $user = $this->getUser();

        //création du formulaire
        $form = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);

        //Si formulaire est envoyé et qu'il est validé
        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'ancien mot de passe
            $old_pwd = $form->get('old_password')->getData();
            //Si l'ancien password est validé
            if ($encoder->isPasswordValid($user, $old_pwd)) {
                //récupération du nouveau mot de passe
                $new_password = $form->get('new_password')->getData();
                //encode le nouveau mot de passe
                $password = $encoder->encodePassword($user, $new_password);
                $user->setPassword($password);
                //enregistre en BDD
                $this->entityManager->flush();
                $notification = 'Le mot de passe a été mise à jour.';
                $success = true;
            } else {
                $notification = 'Votre mot de passe actuel est incorrect.';
            }
        }

        return $this->render('account/password.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification,
            'success' => $success,
        ]);
    }
}
