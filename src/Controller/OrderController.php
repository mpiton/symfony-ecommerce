<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Order;
use App\Entity\OrderDetails;
use App\Form\OrderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class OrderController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/commande", name="order", methods={"POST"})
     */
    public function add(Cart $cart, Request $request): Response
    {
        $form = $this->createForm(OrderType::class, null, [
            'user' => $this->getUser(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $carriers = $form->get('carriers')->getData();
            $delivery = $form->get('addresses')->getData();
            $delivery_content =
                "<div class='card rounded-1'><ul class='card-body' style='list-style-type:none'><li>Nom complet: " .
                $delivery->getFirstname() .
                ' ' .
                $delivery->getLastname() .
                '</li>';
            $delivery_content .=
                '<li>Téléphone: ' . $delivery->getPhone() . '</li>';
            if ($delivery->getCompany()) {
                $delivery_content .=
                    '<li>Entreprise: ' . $delivery->getCompany() . '</li>';
            }
            $delivery_content .=
                '<li>Adresse: ' . $delivery->getAddress() . '</li>';
            $delivery_content .=
                '<li>Code Postal: ' . $delivery->getPostal() . '</li>';
            $delivery_content .=
                '<li>Ville:' .
                ' ' .
                $delivery->getCity() .
                ' ' .
                $delivery->getCountry() .
                '</li></ul></div>';

            //register my order -> Order()
            $order = new Order();
            $reference = $date->format('dmY') . '-' . uniqid();
            $order->setReference($reference);
            $order->setUser($this->getUser());
            $order->setCreatedAt($date);
            $order->setCarrierName($carriers->getName());
            $order->setCarrierPrice($carriers->getPrice());
            $order->setDelivery($delivery_content);
            $order->setState(0);
            $this->entityManager->persist($order);

            //register my product -> OrderDetails()
            foreach ($cart->getFullCart() as $product) {
                $orderDetails = new OrderDetails();
                $orderDetails->setMyOrder($order);
                $orderDetails->setProduct($product['product']->getName());
                $orderDetails->setQuantity($product['quantity']);
                $orderDetails->setPrice($product['product']->getPrice());
                $orderDetails->setTotal(
                    $product['product']->getPrice() * $product['quantity']
                );
                $this->entityManager->persist($orderDetails);
            }
            $this->entityManager->flush();

            return $this->render('order/index.html.twig', [
                'cart' => $cart->getFullCart(),
                'carrier' => $carriers,
                'delivery' => $delivery_content,
                'reference' => $order->getReference(),
            ]);
        }
        //else redirect to cart
        return $this->redirectToRoute('cart');
    }
}
