<?php

namespace App\Form;

use App\Entity\Address;
use App\Entity\Carrier;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $builder
            ->add('addresses', EntityType::class, [
                'label' => 'Votre adresse',
                'required' => true,
                'class' => Address::class,
                'choices' => $user->getAddresses(),
                'multiple' => false,
                'expanded' => false,
            ])
            ->add('carriers', EntityType::class, [
                'label' => 'Livraison',
                'required' => true,
                'class' => Carrier::class,
                'multiple' => false,
                'expanded' => false,
                'attr' => [
                    'class' => 'selectpicker',
                ],
            ])
            ->add('code', TextType::class, [
                'label' => 'Code Promo',
                'attr' => [
                    'placeholder' => 'Code promotionnel',
                ],
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Paiement',
                'attr' => [
                    'class' => 'btn',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'user' => [],
        ]);
    }
}
